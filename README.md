Adoption Rates Plotting
=======================

To run yourself:

    git clone https://gitlab.com/bquigley/adoption-rates
    cd adoption-rates
    python3 -m pip install --user poetry  # if needed
    poetry install
    poetry run python3 main.py

![](output.png
 "Chart of children in foster care waiting to be adopted by US state.")
