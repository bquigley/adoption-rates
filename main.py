import json

from urllib.request import urlopen
import pandas as pd
import plotly.graph_objects as go

from states import US_STATES

def main():
    """Main control flow function."""
    with urlopen(
        "https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json"
    ) as response:
        counties = json.load(response)
    df = pd.read_csv("Children in foster care waiting for adoption.csv")
    df['Location'] = df['Location'].map(US_STATES)
    state_data = df.loc[df['LocationType'] == "State"]
    state_data['Data'] = state_data['Data'].astype(float)
    state_data_2018 = state_data.loc[state_data['TimeFrame'] == 2018]
    fig = go.Figure(
        data=go.Choropleth(
            locations=state_data_2018["Location"],  # Spatial coordinates
            locationmode="USA-states",  # set of locations match entries in `locations`
            z=state_data_2018["Data"],
            colorscale="Mint",
            colorbar_title="children in foster care waiting for adoption",
        ),
    )
    fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
    fig.show()


if __name__ == "__main__":
    main()
